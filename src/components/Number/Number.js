import React from 'react';

const Number = props => {
    return (
        <div className="number">
            <h1>{props.value}</h1>
        </div>
    )
}

export default Number;