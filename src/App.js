import React from "react";
import './App.css';
import Number from "./components/Number/Number";

class App extends React.Component {
    state = {
        numbers: [
            {value: 0},
            {value: 0},
            {value: 0},
            {value: 0},
            {value: 0}
        ]
    }

    changeValue = () => {

        let numbers = [...this.state.numbers];
        let arr = [];
        while (arr.length < 5) {
            let r = Math.floor(Math.random() * 32) + 5;
            if (arr.indexOf(r) === -1) {
                arr.push(r);
            }
        }

        numbers[0].value = arr[0];
        numbers[1].value = arr[1];
        numbers[2].value = arr[2];
        numbers[3].value = arr[3];
        numbers[4].value = arr[4];

        for (let i = 1; i < numbers.length; i++) {
            for (let j = 0; j < i; j++) {
                if (numbers[i].value < numbers[j].value) {
                    let x = numbers[i].value;
                    numbers[i].value = numbers[j].value;
                    numbers[j].value = x;
                }
            }
        }
        this.setState({numbers});
    };

    render() {
        return (
            <>
                <div className="App">
                    <div>
                        <button onClick={this.changeValue}>New numbers</button>
                    </div>
                    <Number value={this.state.numbers[0].value}/>
                    <Number value={this.state.numbers[1].value}/>
                    <Number value={this.state.numbers[2].value}/>
                    <Number value={this.state.numbers[3].value}/>
                    <Number value={this.state.numbers[4].value}/>
                </div>
            </>
        );
    }
}

export default App;
